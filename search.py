# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
from util import Stack, Queue, PriorityQueue
from sets import Set

"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    explored = Set()
    frontier = Stack()
    frontier.push(problem.getStartState())
    parent = {}
    
    while not frontier.isEmpty(): 
        current_node = frontier.pop()
        explored.add(current_node)

        # Goal Test
        if problem.isGoalState(current_node):
            #Sequence of actions that leads to the right solution
            path = []
            while current_node in parent.keys():
                direct_parent, action = parent[current_node]
                path.append(action)
                current_node = direct_parent

            path.reverse()
            return path
            
        
        for child,action,cost in problem.getSuccessors(current_node):
            if child not in explored:
                #add state description to frontier
                frontier.push(child)
                
                # update parent map with child -> parent, action
                parent[child] = (current_node, action)

    return []

def breadthFirstSearch(problem):
    explored = Set()
    frontier = Queue()
    frontier.push(problem.getStartState())
    parent = {}
    parent[problem.getStartState()] = (problem.getStartState(), None)
    while not frontier.isEmpty(): 
        current_node = frontier.pop()

        # Goal Test
        if problem.isGoalState(current_node):
            #Sequence of actions that leads to the right solution
            path = []
            while current_node != parent[current_node][0]:
                direct_parent, action = parent[current_node]
                path.append(action)
                current_node = direct_parent
            
            path.reverse()
            return path
            
        explored.add(current_node)
        for child,action,cost in problem.getSuccessors(current_node):
            if child not in explored and child not in frontier.list:
                #add state description to frontier
                frontier.push(child)
                
                # update parent map with child -> parent, action
                parent[child] = (current_node, action)

    return []

def uniformCostSearch(problem):
    node_color = {}
    frontier = PriorityQueue()
    parent = {}
    min_cost = {}
    frontier.push(problem.getStartState(), 0)
    min_cost[problem.getStartState()] = 0
    
    while not frontier.isEmpty():
        current_node = frontier.pop()
        node_color[current_node] = 'red'

        # Goal Test
        if problem.isGoalState(current_node):
            #Sequence of actions that leads to the right solution
            path = []
            while current_node in parent.keys():
                direct_parent, action = parent[current_node]
                path.append(action)
                current_node = direct_parent

            path.reverse()
            return path
                    
        for child, action, cost in problem.getSuccessors(current_node):
            if child not in node_color.keys():
                #add state description to frontier
                frontier.push(child, cost + min_cost[current_node])
                min_cost[child] = cost + min_cost[current_node]
                node_color[child] = 'grey'
                
                # update parent map with child -> parent, action
                parent[child] = (current_node, action)
            elif child in node_color.keys() and node_color[child] is 'grey':
                if(cost + min_cost[current_node] < min_cost[child]):
                    parent[child] = (current_node, action)
                    min_cost[child] = cost + min_cost[current_node]
                frontier.update(child, min_cost[child])
    return []

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    node_color = {}
    frontier = PriorityQueue()
    parent = {}
    min_cost = {}
    frontier.push(problem.getStartState(), 0)
    min_cost[problem.getStartState()] = 0
    
    while not frontier.isEmpty():
        current_node = frontier.pop()
        node_color[current_node] = 'red'

        # Goal Test
        if problem.isGoalState(current_node):
            #Sequence of actions that leads to the right solution
            path = []
            while current_node in parent.keys():
                direct_parent, action = parent[current_node]
                path.append(action)
                current_node = direct_parent

            path.reverse()
            return path

        for child, action, cost in problem.getSuccessors(current_node):
            if child not in node_color.keys():
                #add state description to frontier
                frontier.push(child, heuristic(child, problem) + min_cost[current_node] + cost)
                min_cost[child] = cost + min_cost[current_node]
                node_color[child] = 'grey'
                
                # update parent map with child -> parent, action
                parent[child] = (current_node, action)
            elif child in node_color.keys() and node_color[child] is 'grey':
                if(cost + min_cost[current_node] < min_cost[child]):
                    parent[child] = (current_node, action)
                    min_cost[child] = cost + min_cost[current_node]
                frontier.update(child, heuristic(child, problem) + min_cost[child])
    return []

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
